from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
from lxml import html
import sys
import os

currentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append("/home/manage_report")

#from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self,  parser_name: str):
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    #@magicDB
    def run(self):
        content: list = self.get_content()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def prepare_item_date(self, date):
        months = {
            'янв': '01',
            'фев': '02',
            'мар': '03',
            'апр': '04',
            'май': '05',
            'июн': '06',
            'июл': '07',
            'авг': '08',
            'сен': '09',
            'окт': '10',
            'ноя': '11',
            'дек': '12',
        }
        day = date.split(' ')[0]
        month = date.split(' ')[1]
        year = date.split(' ')[2]
        month = month.replace(month, months[month]).strip()
        date = '.'.join([day, month, year])

        return date

    def get_urls(self):
        page = 1

        urls = {
            f'https://www.fabrikant.ru/trades/procedure/search/?type=0&org_type=org&currency=0&date_type=date_publication&ensure=all&filter_id=4&okpd2_embedded=1&okdp_embedded=1&count_on_page=40&order_direction=1&type_hash=1561441166&page={page}',
            f'https://www.fabrikant.ru/trades/procedure/search/?type=0&org_type=org&currency=0&date_type=date_publication&ensure=all&filter_id=3&okpd2_embedded=1&okdp_embedded=1&count_on_page=40&order_direction=1&type_hash=1561441166&page={page}',
            f'https://www.fabrikant.ru/trades/procedure/search/?type=0&org_type=org&currency=0&date_type=date_publication&ensure=all&filter_id=2&okpd2_embedded=1&okdp_embedded=1&count_on_page=40&order_direction=1&type_hash=1561441166&page={page}',
            f'https://www.fabrikant.ru/trades/procedure/search/?type=0&org_type=org&currency=0&date_type=date_publication&ensure=all&filter_id=16&okpd2_embedded=1&okdp_embedded=1&count_on_page=40&order_direction=1&type_hash=1561441166&page={page}'
        }

        date_from = str(datetime.strftime((datetime.now() - timedelta(hours=1)), '%d.%m.%Y %H:00'))
        date_to = datetime.strftime((datetime.now()), '%d.%m.%Y %H:00')
        print('from: {}, to: {}'.format(date_from, date_to))

        session = requests.Session()
        session.headers.update({
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
        })
        ads = []
        
        while page < 4:
            for url in urls:
                response = session.get(url, timeout=60)
                soup = BeautifulSoup(response.text, 'lxml')
                market = soup.select_one('section[class=marketplace-list]')
                try:
                    for items in market.find_all('div', class_='innerGrid'):
                        try:
                            item = items.find('div', class_='marketplace-unit ready')
                            newurls = item.find('div', class_='marketplace-unit__cut-wrap').find('h4').find('a').get('href')
                            locdate = item.find('div', class_='marketplace-unit__state__wrap').find('div').find_all('span')[1].get_text()
                            loctime = item.find('div', class_='marketplace-unit__state__wrap').find('div').find_all('span')[2].get_text()
                            new_date = self.prepare_item_date(locdate)
                            resul_date = ' '.join([new_date, loctime])
                            rdate = datetime.strptime(resul_date, '%d.%m.%Y %H:%M')
                            date_f = datetime.strptime(date_from, '%d.%m.%Y %H:%M')
                            date_t = datetime.strptime(date_to, '%d.%m.%Y %H:%M')
                            #если дата входит в промежуток, то записываем
                            if date_f < rdate and rdate < date_t:
                                if newurls not in ads:
                                    ads.append(newurls)
                        except:
                            continue
                except:
                    continue
            page += 1
        return ads        

    def get_content(self):
        ads = self.get_urls()
        contents = []
        for link in ads:
            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'lots': [],
                'attachments': [],
                'procedureInfo': {
                                'endDate': '',
                                'scoringDate': '',
                                'biddingDate': ''
                                },

                'customer': {
                                'fullName': '',
                                'factAddress': '',
                                'inn': 0,
                                'kpp': 0,
                            },
                'contactPerson': {
                                'lastName': '',
                                'firstName': '',
                                'middleName': '',
                                'contactPhone': '',
                                'contactEMail': '',
                                }
            }
            session = requests.Session()
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
            })
            #procedure
            if 'v2/trades/procedure' in link:
                try:
                    response = session.get(link, timeout=60)
                    tree = html.document_fromstring(response.content)

                    item_data['title'] = str(self.get_title_procedure(tree))
                    item_data['url'] = str(link)
                    item_data['purchaseType'] = str(self.get_type_procedure(tree))
                    item_data['purchaseNumber'] = str(self.get_number_procedure(tree))

                    item_data['customer']['fullName'] = str(self.get_customer_procedure(tree))
                    item_data['customer']['factAddress'] = str(self.get_adress_procedure(tree))
                    item_data['customer']['inn'] = int(self.get_inn_procedure(tree))
                    item_data['customer']['kpp'] = int(self.get_kpp_procedure(tree))

                    last_name, first_name, middle_name = self.get_names_procedure(tree)
                    item_data['contactPerson']['lastName'] = str(last_name)
                    item_data['contactPerson']['firstName'] = str(first_name)
                    item_data['contactPerson']['middleName'] = str(middle_name)

                    item_data['contactPerson']['contactPhone'] = str(self.get_phone_procedure(tree))
                    item_data['contactPerson']['contactEMail'] = str(self.get_email_procedure(tree))

                    item_data['procedureInfo']['endDate'] = self.get_end_date_procedure(tree)
                    item_data['procedureInfo']['scoringDate'] = self.get_scor_date_procedure(tree)

                    item_data['lots'] = self.get_lots_procedure(tree)

                    link_procedure = 'https://www.fabrikant.ru/' + ''.join(tree.xpath('//*[contains(text(),"Документация")]/parent::*/@href'))
                    item_data['attachments'] = self.get_attach_procedure(link_procedure)

                    contents.append(item_data)
                except Exception as e:
                    print(f'ERROR: {e}. ON PAGE: {link}')

            #corpotare
            elif 'trades/corporate/ProcedurePurchase' in link:
                try:
                    response = session.get(link, timeout=60)
                    tree = html.document_fromstring(response.content)

                    item_data['title'] = str(self.get_title_corpotare(tree))
                    item_data['url'] = str(link)
                    item_data['purchaseType'] = str(self.get_type_corpotare(tree))
                    item_data['purchaseNumber'] = str(self.get_number_corpotare(tree))

                    item_data['customer']['fullName'] = str(self.get_customer_corpotare(tree))
                    item_data['customer']['factAddress'] = str(self.get_adress_corpotare(tree))
                    item_data['customer']['inn'] = int(self.get_inn_corpotare(tree))
                    item_data['customer']['kpp'] = int(self.get_kpp_corpotare(tree))

                    last_name, first_name, middle_name = self.get_names_corpotare(tree)
                    item_data['contactPerson']['lastName'] = str(last_name)
                    item_data['contactPerson']['firstName'] = str(first_name)
                    item_data['contactPerson']['middleName'] = str(middle_name)

                    item_data['contactPerson']['contactPhone'] = str(self.get_phone_corpotare(tree))
                    item_data['contactPerson']['contactEMail'] = str(self.get_email_corpotare(tree))

                    item_data['procedureInfo']['endDate'] = self.get_end_date_corpotare(tree)
                    item_data['procedureInfo']['scoringDate'] = self.get_scor_date_corpotare(tree)

                    item_data['lots'] = self.get_lots_corpotare(tree)

                    link_id = ''.join(link.split('id=')[1].split('#lot_1')[0])
                    link_corporate = 'https://www.fabrikant.ru/trades/corporate/ProcedurePurchase/?action=file_documentations_view&procedure_id=' + link_id
                    item_data['attachments'] = self.get_attach_corpotare(link_corporate)
                    contents.append(item_data)
                except Exception as e:
                    print(f'ERROR: {e}. ON PAGE: {link}')

            #market
            elif 'market/view' in link:
                try:
                    response = session.get(link, timeout=60)
                    tree = html.document_fromstring(response.content)

                    item_data['title'] = str(self.get_title_market(tree))
                    item_data['url'] = str(link)
                    item_data['purchaseType'] = str(self.get_type_market(tree))
                    item_data['purchaseNumber'] = str(self.get_number_market(tree))

                    item_data['customer']['fullName'] = str(self.get_customer_market(tree))
                    item_data['customer']['factAddress'] = str(self.get_adress_market(tree))
                    item_data['customer']['inn'] = int(self.get_inn_market(tree))
                    item_data['customer']['kpp'] = int(self.get_kpp_market(tree))

                    last_name, first_name, middle_name = self.get_names_market(tree)
                    item_data['contactPerson']['lastName'] = str(last_name)
                    item_data['contactPerson']['firstName'] = str(first_name)
                    item_data['contactPerson']['middleName'] = str(middle_name)

                    item_data['contactPerson']['contactPhone'] = str(self.get_phone_market(tree))
                    item_data['contactPerson']['contactEMail'] = str(self.get_email_market(tree))

                    item_data['procedureInfo']['endDate'] = self.get_end_date_market(tree)
                    item_data['procedureInfo']['scoringDate'] = self.get_scor_date_market(tree)

                    item_data['lots'] = self.get_lots_market(tree)

                    link_id = ''.join(link.split('id=')[1])
                    link_market = 'https://www.fabrikant.ru/market/view.html?action=view_auction_documentation&id=' + link_id
                    item_data['attachments'] = self.get_attach_market(link_market)

                    contents.append(item_data)
                except Exception as e:
                    print(f'ERROR: {e}. ON PAGE: {link}')

            #atom
            elif 'trades/atom' in link:
                try:
                    response = session.get(link, timeout=60)
                    tree = html.document_fromstring(response.content)

                    item_data['title'] = str(self.get_title_atom(tree))
                    item_data['url'] = str(link)
                    item_data['purchaseType'] = str(self.get_type_atom(tree))
                    item_data['purchaseNumber'] = str(self.get_number_atom(tree))

                    item_data['customer']['fullName'] = str(self.get_customer_atom(tree))
                    item_data['customer']['factAddress'] = str(self.get_adress_atom(tree))
                    item_data['customer']['inn'] = int(self.get_inn_atom(tree))
                    item_data['customer']['kpp'] = int(self.get_kpp_atom(tree))

                    last_name, first_name, middle_name = self.get_names_atom(tree)
                    item_data['contactPerson']['lastName'] = str(last_name)
                    item_data['contactPerson']['firstName'] = str(first_name)
                    item_data['contactPerson']['middleName'] = str(middle_name)

                    item_data['contactPerson']['contactPhone'] = str(self.get_phone_atom(tree))
                    item_data['contactPerson']['contactEMail'] = str(self.get_email_atom(tree))

                    item_data['procedureInfo']['endDate'] = self.get_end_date_atom(tree)
                    item_data['procedureInfo']['scoringDate'] = self.get_scor_date_atom(tree)
                    item_data['procedureInfo']['biddingDate'] = self.get_bid_date_atom(tree)

                    item_data['lots'] = self.get_lots_atom(tree)

                    link_id = ''.join(link.split('id=')[1])
                    link_atom = 'https://www.fabrikant.ru/trades/atom/ProposalRequest/?action=file_documentations_view&procedure_id=' + link_id
                    item_data['attachments'] = self.get_attach_atom(link_atom)

                    contents.append(item_data)
                except Exception as e:
                    print(f'ERROR: {e}. ON PAGE: {link}')

            #catalog
            elif 'catalog/procedure?' in link:
                try:
                    response = session.get(link, timeout=60)
                    html_doc = html.document_fromstring(response.content)

                    new_link = ''.join(html_doc.xpath('//td[@class="row-procedureName "]/a/@href'))
                    new_response = session.get(new_link, timeout=60)
                    tree = html.document_fromstring(new_response.content)

                    item_data['title'] = str(self.get_title_catalog(tree))
                    item_data['url'] = str(link)
                    item_data['purchaseType'] = str(self.get_type_catalog(tree))
                    item_data['purchaseNumber'] = str(self.get_number_catalog(tree))

                    item_data['customer']['fullName'] = str(self.get_customer_catalog(tree))
                    item_data['customer']['factAddress'] = str(self.get_adress_catalog(tree))
                    item_data['customer']['inn'] = int(self.get_inn_catalog(tree))
                    item_data['customer']['kpp'] = int(self.get_kpp_catalog(tree))

                    last_name, first_name, middle_name = self.get_names_catalog(tree)
                    item_data['contactPerson']['lastName'] = str(last_name)
                    item_data['contactPerson']['firstName'] = str(first_name)
                    item_data['contactPerson']['middleName'] = str(middle_name)

                    item_data['contactPerson']['contactPhone'] = str(self.get_phone_catalog(tree))
                    item_data['contactPerson']['contactEMail'] = str(self.get_email_catalog(tree))

                    item_data['procedureInfo']['endDate'] = self.get_end_date_catalog(tree)
                    item_data['procedureInfo']['scoringDate'] = self.get_scor_date_catalog(tree)

                    link_catalog_lot = ''.join(new_link.replace('/rosatom/procedure/view/', '/rosatom/lot/viewList/').split('?&backurl=')[0])
                    item_data['lots'] = self.get_lots_catalog(link_catalog_lot, tree)

                    link_catalog_attach = ''.join(new_link.replace('/rosatom/procedure/view/', '/rosatom/documentation/view/').split('?&backurl=')[0])
                    item_data['attachments'] = self.get_attach_catalog(link_catalog_attach)

                    contents.append(item_data)
                except Exception as e:
                    print(f'ERROR: {e}. ON PAGE: {link}')
            else:
                pass
        return contents

    def get_title_procedure(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Общее наименование закупки")]/parent::*/./*[2]/text()')).strip() or ''.join(tree.xpath('//*[contains(text(),"Наименование запроса")]/parent::*/./*[2]/text()')).strip() or ''.join(tree.xpath('//*[contains(text(),"Предмет закупки")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_type_procedure(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Тип процедуры")]/parent::*/./*[2]/text()')).strip() or ''.join(tree.xpath('//*[contains(text(),"Способ проведения процедуры")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_number_procedure(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Процедурная часть")]/text()[1]')).strip()
        except:
            data = ''
        return str(data)

    def get_customer_procedure(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Информация об организаторе")]/parent::*/./*[2]/./*[1]/a/text()[1]')).strip() or ''.join(tree.xpath('//*[contains(text(),"Организатор мониторинга цен")]/parent::*/./*[2]/./*[1]/a/text()')).strip() or ''.join(tree.xpath('//*[contains(text(),"Заказчик")]/parent::*/./*[2]/./*[1]/a/text()[1]')).strip()
        except:
            data = ''
        return str(data)

    def get_adress_procedure(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Почтовый адрес")]/parent::*/text()')).strip() or ''.join(tree.xpath('//*[contains(text(),"Юридический адрес")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_inn_procedure(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"ИНН")]/parent::*/text()')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_kpp_procedure(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"КПП")]/parent::*/text()')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_names_procedure(self, tree):
        try:
            contact = ''.join(tree.xpath('//*[contains(text(),"Контактное лицо")]/parent::*/text()')).strip()

            try:
                last_name = ''.join(contact.split()[0])
            except:
                last_name = ''
            try:
                first_name = ''.join(contact.split()[1])
            except:
                first_name = ''
            try:
                middle_name = ''.join(contact.split()[2])
            except:
                middle_name = ''
        except:
            last_name = ''
            first_name = ''
            middle_name = ''
        return str(last_name), str(first_name), str(middle_name)

    def get_phone_procedure(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Телефон")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_email_procedure(self, tree):
        try:
            data = ''.join(
                tree.xpath('//*[contains(text(),"Email")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_end_date_procedure(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата и время окончания приема заявок")]/parent::*/./*[2]/text()[1]')).strip() or ''.join(tree.xpath('//*[contains(text(),"Дата окончания приёма предложений")]/parent::*/./*[2]/text()[1]')).strip() or ''.join(tree.xpath('//*[contains(text(),"Дата завершения приема предложений")]/parent::*/./*[2]/text()[1]')).strip() or ''.join(tree.xpath('//*[contains(text(),"Дата и время начала проведения конкурентного отбора")]/parent::*/./*[2]/text()[1]')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_scor_date_procedure(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата и время окончания подведения итогов")]/parent::*/./*[2]/text()[1]')).strip() or ''.join(tree.xpath('//*[contains(text(),"Дата подведения итогов процедур")]/parent::*/./*[2]/text()[1]')).strip() or ''.join(tree.xpath('//*[contains(text(),"Дата и время окончания проведения конкурентного отбора")]/parent::*/./*[2]/text()[1]')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_lots_procedure(self, tree):
        try:
            data = []
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []

            }
            code = ' '.join(''.join(tree.xpath('//table[@class="table table-bordered table-striped"]/tbody/tr/td[1]/text()')).split()) or '1'
            name = ' '.join(''.join(tree.xpath('//table[@class="table table-bordered table-striped"]/tbody/tr/td[2]/text()[1]').strip()).split()) or ''.join(tree.xpath('//*[contains(text(),"Предмет договора")]/parent::*/./*[2]/text()')).strip() or ''
            names = {'code': code,
                     'name': name}

            lots['address'] = ' '.join(''.join(tree.xpath('//table[@class="table table-bordered table-striped"]/tbody/tr/td[3]/text()[1]')).strip().split()) or ''.join(tree.xpath('//*[contains(text(),"Место поставки товара")]/parent::*/parent::*/text()[1]')).strip() or ''.join(tree.xpath('//*[contains(text(),"Место поставки")]/parent::*/./*[2]/text()')).strip() or ''
            lots['price'] = ' '.join(''.join(tree.xpath('//table[@class="table table-bordered table-striped"]/tbody/tr/td[5]/text()[1]')).strip().split()) or ' '.join(''.join(tree.xpath('//*[contains(text(),"Начальная цена")]/parent::*/./*[2]/div/text()')).strip().split()) or ' '.join(''.join(tree.xpath('//*[contains(text(),"Начальная (максимальная) цена договора")]/parent::*/./*[2]/div/text()[2]')).strip().split()) or ''
            lots['lotItems'].append(names)
            data.append(lots)
        except:
            data = []
        return data

    def get_attach_procedure(self, link):
        try:
            session = requests.Session()
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
            })
            response = session.get(link, timeout=60)
            tree = html.document_fromstring(response.content)
            data = []
            doc_div = tree.xpath('//table[@class = "table table-default table-bordered table-striped table-condensed tab-doc"][1]/tbody[1]/tr')

            for doc in doc_div:
                docs = {'docDescription': '', 'url': ''}

                descript = ''.join(''.join(doc.xpath('td[1]/text()')).strip().split())
                url = doc.xpath('td[6]/div/a/@href')

                docs['docDescription'] = descript
                docs['url'] = 'https://www.fabrikant.ru/' + ''.join(url)

                if descript == 'Нетзагруженныхдокументов':
                    break

                data.append(docs)
        except:
            data = []
        return data

    def get_title_corpotare(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Общее наименование закупки")]/parent::*/./*[2]/./*[1]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_type_corpotare(self, tree):
        try:
            data = ''.join(
                tree.xpath('//*[contains(text(),"Нормативные рамки закупки")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_number_corpotare(self, tree):
        try:
            data = ''.join(''.join(''.join(tree.xpath('//*[contains(text()," № ")]/text()[1]')).strip().split('№')[1]).split('"')[0])
        except:
            data = ''
        return str(data)

    def get_customer_corpotare(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Организатор:")]/parent::*/./*[2]/./*[1]/./*[1]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_adress_corpotare(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Организатор:")]/parent::*/./*[2]/./*[1]/text()[2]')).strip()[2:]
        except:
            data = ''
        return str(data)

    def get_inn_corpotare(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"ИНН")]/parent::*/text()')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_kpp_corpotare(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"КПП")]/parent::*/text()')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_names_corpotare(self, tree):
        try:
            contact = ''.join(tree.xpath('//*[contains(text(),"Контактное лицо")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()

            try:
                last_name = ''.join(contact.split()[0])
            except:
                last_name = ''
            try:
                first_name = ''.join(contact.split()[1])
            except:
                first_name = ''
            try:
                middle_name = ''.join(contact.split()[2])
            except:
                middle_name = ''
        except:
            last_name = ''
            first_name = ''
            middle_name = ''
        return str(last_name), str(first_name), str(middle_name)

    def get_phone_corpotare(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Телефон")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_email_corpotare(self, tree):
        try:
            data = ''.join(
                tree.xpath('//*[contains(text(),"Email")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_end_date_corpotare(self, tree):
        try:
            data = ''.join(tree.xpath(
                '//*[contains(text(),"Дата и время окончания приема заявок")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_scor_date_corpotare(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата и время подведения итогов закупки")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_lots_corpotare(self, tree):
        try:
            data = []
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []
            }
            code = '1'
            name = ' '.join(''.join(tree.xpath('//*[contains(text(),"Тип предмета закупки:")]/parent::*/./*[2]/./*[1]/text()[1]')).split())
            names = {'code': code,
                     'name': name}

            lots['address'] = ' '.join(''.join(tree.xpath('//*[contains(text(),"Место поставки товара:")]/parent::*/./*[2]/./*[1]/text()[1]')).strip().split())
            lots['price'] = ' '.join(''.join(tree.xpath('//*[contains(text(),"Цена:")]/parent::*/./*[2]/text()')).strip().split())
            lots['lotItems'].append(names)
            data.append(lots)
        except:
            data = []
        return data

    def get_attach_corpotare(self, link):
        try:
            session = requests.Session()
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
            })
            response = session.get(link, timeout=60)
            tree = html.document_fromstring(response.content)
            data = []
            doc_div = tree.xpath('//table[@class="list document_list"][1]/tbody/tr')

            for doc in doc_div[1:]:
                docs = {'docDescription': '', 'url': ''}

                descript = ' '.join(''.join(doc.xpath('td[2]/a/b/text()')).strip().split())
                url = doc.xpath('td[2]/a/@href')

                docs['docDescription'] = descript
                docs['url'] = ''.join(url)

                data.append(docs)
        except:
            data = []
        return data

    def get_title_market(self, tree):
        try:
            data = ''.join(tree.xpath(
                '//*[contains(text(),"Общее наименование закупки")]/parent::*/./*[2]/text()')).strip() or ''.join(
                tree.xpath('//*[contains(text(),"Наименование запроса")]/parent::*/./*[2]/text()')).strip() or ''.join(
                tree.xpath('//*[contains(text(),"Предмет закупки")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_type_market(self, tree):
        try:
            data = ''.join(
                tree.xpath('//*[contains(text(),"Тип процедуры")]/parent::*/./*[2]/text()')).strip() or ''.join(
                tree.xpath('//*[contains(text(),"Способ проведения процедуры")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_number_market(self, tree):
        try:
            data = ''.join(''.join(''.join(tree.xpath('//*[contains(text(),"торговой процедуры")]/text()[1]')).strip().split('№')[1]).split('"')[0])
        except:
            data = ''
        return str(data)

    def get_customer_market(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Заказчик")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_adress_market(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Почтовый адрес")]/parent::*/text()')).strip() or ''.join(
                tree.xpath('//*[contains(text(),"Юридический адрес")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_inn_market(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"ИНН")]/parent::*/text()')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_kpp_market(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"КПП")]/parent::*/text()')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_names_market(self, tree):
        try:
            contact = ''.join(tree.xpath('//*[contains(text(),"Контактное лицо")]/parent::*/./*[2]/text()[1]')).strip()

            try:
                last_name = ''.join(contact.split()[0])
            except:
                last_name = ''
            try:
                first_name = ''.join(contact.split()[1])
            except:
                first_name = ''
            try:
                middle_name = ''.join(contact.split()[2])
            except:
                middle_name = ''
        except:
            last_name = ''
            first_name = ''
            middle_name = ''
        return str(last_name), str(first_name), str(middle_name)

    def get_phone_market(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Телефон")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_email_market(self, tree):
        try:
            data = ''.join(
                tree.xpath('//*[contains(text(),"Email")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_end_date_market(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата завершения процедуры")]/parent::*/./*[2]/./*[1]/text()')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_scor_date_market(self, tree):
        try:
            data = ''.join(tree.xpath(
                '//*[contains(text(),"Дата и время окончания подведения итогов")]/parent::*/./*[2]/text()[1]')).strip() or ''.join(
                tree.xpath(
                    '//*[contains(text(),"Дата подведения итогов процедур")]/parent::*/./*[2]/text()[1]')).strip() or ''.join(
                tree.xpath(
                    '//*[contains(text(),"Дата и время окончания проведения конкурентного отбора")]/parent::*/./*[2]/text()[1]')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_lots_market(self, tree):
        try:
            data = []
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []

            }
            code = '1'
            name = ' '.join(''.join(tree.xpath('//*[@class="list"]/tbody/tr[2]/td[2]/p/text()')).split())
            names = {'code': code,
                     'name': name}

            lots['address'] = ' '.join(''.join(tree.xpath('//*[contains(text(),"Условия поставки")]/parent::*/./*[2]/text()')).strip().split())
            lots['price'] = ' '.join(''.join(tree.xpath('//*[@class="list"]/tbody/tr[2]/td[6]/text()')).strip().split())
            lots['lotItems'].append(names)
            data.append(lots)
        except:
            data = []
        return data

    def get_attach_market(self, link):
        try:
            session = requests.Session()
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
            })
            response = session.get(link, timeout=60)
            tree = html.document_fromstring(response.content)

            data = []
            doc_div = tree.xpath('//table[@class="list"][1]/tbody/tr')
            for doc in doc_div:
                docs = {'docDescription': '', 'url': ''}

                descript = ''.join(''.join(doc.xpath('td[2]/a/b/text()')).strip().split())
                url = doc.xpath('td[2]/a/@href')

                docs['docDescription'] = descript
                docs['url'] = 'https://www.fabrikant.ru/' + ''.join(url)

                data.append(docs)
        except:
            data = []
        return data

    def get_title_atom(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Общее наименование закупки")]/parent::*/./*[2]/./*[1]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_type_atom(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Форма и способ процедуры закупки")]/parent::*/./*[2]/./*[1]/text()')).strip() or ''.join(tree.xpath('//*[contains(text(),"Форма процедуры закупки")]/parent::*/./*[2]/./*[1]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_number_atom(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Номер закупки на официальном сайте ГК «Росатом»")]/parent::*/./*[2]/./*[1]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_customer_atom(self, tree):
        try:
            data = ' '.join(''.join(tree.xpath('//*[contains(text(),"Организатор")]/parent::*/./*[2]/./*[1]/a/text()')).strip().split())
        except:
            data = ''
        return str(data)

    def get_adress_atom(self, tree):
        try:
            data = ' '.join(''.join(tree.xpath('//*[contains(text(),"Организатор")]/parent::*/./*[2]/./*[1]/text()[2]')).strip()[2:].split())
        except:
            data = ''
        return str(data)

    def get_inn_atom(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"ИНН")]/parent::*/text()')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_kpp_atom(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"КПП")]/parent::*/text()')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_names_atom(self, tree):
        try:
            contact = ''.join(tree.xpath('//*[contains(text(),"Контактное лицо")]/parent::*/./*[2]/./*[1]/text()[2]')).strip()
            words = ['Специалист', 'Инженер', 'Ведущий', 'Начальник', 'Главный', 'Экономист', 'Руководитель', 'Первый']
            if any(map(contact.lower().__contains__, map(str.lower, words))):
                contact = ''.join(tree.xpath('//*[contains(text(),"Контактное лицо")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()

            try:
                last_name = ''.join(contact.split()[0])
            except:
                last_name = ''
            try:
                first_name = ''.join(contact.split()[1])
            except:
                first_name = ''
            try:
                middle_name = ''.join(contact.split()[2])
            except:
                middle_name = ''
        except:
            last_name = ''
            first_name = ''
            middle_name = ''
        return str(last_name), str(first_name), str(middle_name)

    def get_phone_atom(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Телефон")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_email_atom(self, tree):
        try:
            data = ''.join(
                tree.xpath('//*[contains(text(),"Email")]/parent::*/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_end_date_atom(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата и время окончания приёма предложений на участие")]/parent::*/./*[2]/./*[1]/strong/text()')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_scor_date_atom(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата и время рассмотрения предложений на участие в запросе предложений")]/parent::*/./*[2]/./*[1]/strong/text()')).strip() or ''.join(tree.xpath('//*[contains(text(),"Дата подведения итогов")]/parent::*/./*[2]/./*[1]/strong/text()')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_bid_date_atom(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Срок заключения договора после определения победителя")]/parent::*/./*[2]/./*[1]/strong/text()')).strip() or ''.join(tree.xpath('//*[contains(text(),"Срок заключения договора")]/parent::*/./*[2]/./*[1]/b/text()')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_lots_atom(self, tree):
        try:
            data = []
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []

            }
            code = '1'
            name = ' '.join(''.join(tree.xpath('//*[contains(text(),"Предмет закупки")]/parent::*/./*[2]/./*[1]/text()')).split())
            names = {'code': code,
                     'name': name}

            lots['address'] = ' '.join(''.join(tree.xpath('//*[contains(text(),"Условия поставки")]/parent::*/./*[2]/./*[1]/text()')).strip().split())
            lots['price'] = ' '.join(''.join(tree.xpath('//*[contains(text(),"Начальная (максимальная) цена договора")]/parent::*/./*[2]/./*[1]/text()')).strip().split())
            lots['lotItems'].append(names)
            data.append(lots)
        except:
            data = []
        return data

    def get_attach_atom(self, link):
        try:
            session = requests.Session()
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
            })
            response = session.get(link, timeout=60)
            tree = html.document_fromstring(response.content)
            data = []
            doc_div = tree.xpath('//table[@class="list document_list"]/tbody/tr')

            for doc in doc_div[1:]:
                docs = {'docDescription': '', 'url': ''}

                descript = ''.join(''.join(doc.xpath('td/a/b/text()')).strip().split())
                url = doc.xpath('td/a/@href')

                docs['docDescription'] = descript
                docs['url'] = 'https://www.fabrikant.ru/' + ''.join(url)

                data.append(docs)
        except:
            data = []
        return data

    def get_title_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Наименование закупки")]/parent::*/./*[2]/./*[1]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_type_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Способ закупки")]/parent::*/./*[2]/./*[1]/text()')).strip()
        except:
            data = ''
        return str(data)

    def get_number_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Номер извещения")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
        except:
            data = ''
        return str(data)

    def get_customer_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Наименование организации")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
        except:
            data = ''
        return str(data)

    def get_adress_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Почтовый адрес")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
        except:
            data = ''
        return str(data)

    def get_inn_catalog(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"ИНН")]/parent::*/./*[2]/./*[1]/text()[1]')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_kpp_catalog(self, tree):
        try:
            data = ''.join(''.join(tree.xpath('//*[contains(text(),"КПП")]/parent::*/./*[2]/./*[1]/text()[1]')).strip().split()[0])
            if data.isdigit() and data:
                data = data
            else:
                data = 0
        except:
            data = 0
        return int(data)

    def get_names_catalog(self, tree):
        try:
            last_name = ''.join(tree.xpath('//*[contains(text(),"Фамилия")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
        except:
            last_name = ''
        try:
            first_name = ''.join(tree.xpath('//*[contains(text(),"Имя")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
        except:
            first_name = ''
        try:
            middle_name = ''.join(tree.xpath('//*[contains(text(),"Отчество")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
        except:
            middle_name = ''

        return str(last_name), str(first_name), str(middle_name)

    def get_phone_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Телефон")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
        except:
            data = ''
        return str(data)

    def get_email_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Электронная почта")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
        except:
            data = ''
        return str(data)

    def get_end_date_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата и время окончания срока")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_scor_date_catalog(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата и время подведения")]/parent::*/./*[2]/./*[1]/text()[1]')).strip()
            formatted_date = self.formate_date(data[0:10])
        except:
            formatted_date = None
        return formatted_date

    def get_lots_catalog(self, link, first_tree):
        try:
            session = requests.Session()
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
            })
            response = session.get(link, timeout=60)
            tree = html.document_fromstring(response.content)
            data = []
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []

            }
            code = '1'
            name = ' '.join(''.join(tree.xpath('//table[@class="mte-grid-table table table-bordered table-striped table-hover"]/tbody/tr/td[2]/div/a/text()')).split())
            names = {'code': code,
                     'name': name}

            lots['address'] = ' '.join(''.join(first_tree.xpath('//*[contains(text(),"Место подведения итогов")]/parent::*/./*[2]/./*[1]/text()[1]')).strip().split())
            lots['price'] = ' '.join(''.join(tree.xpath('//table[@class="mte-grid-table table table-bordered table-striped table-hover"]/tbody/tr/td[4]/div/text()')).strip().split())
            lots['lotItems'].append(names)
            data.append(lots)
        except:
            data = []
        return data

    def get_attach_catalog(self, link):
        try:
            session = requests.Session()
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
            })
            response = session.get(link, timeout=60)
            tree = html.document_fromstring(response.content)
            data = []
            doc_div = tree.xpath('//table[@id="procedureDocuments"]/tbody/tr')

            for doc in doc_div:
                docs = {'docDescription': '', 'url': ''}

                descript = ''.join(''.join(doc.xpath('td[2]/div/p/a/text()')).strip().split())
                url = doc.xpath('td[2]/div/p/a/@href')

                docs['docDescription'] = descript
                docs['url'] = ''.join(url)

                data.append(docs)
        except:
            data = []
        return data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
